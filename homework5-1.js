let fs = require("fs")

fs.readFile('homework1-4.json', 'utf8', (err, data) => {
    let jsonData = JSON.parse(data)

    let obj = {}
    let brown =0, blue = 0, green = 0
    for (const key in jsonData) {
      
        if(jsonData[key].eyeColor == "brown") {
            brown++
        }
        if(jsonData[key].eyeColor == "green") {
            green++
        }
        if(jsonData[key].eyeColor == "blue") {
            blue++
        }
    
    }
    obj.brown = brown
    obj.green = green
    obj.blue = blue

    fs.writeFile('homework5-1_eyes.json', JSON.stringify(obj) , 'utf8', (err) => {
        if(err) {
            console.log(err)
        } else {
            console.log("write success")
        }
    })

    let obj1 = {}
    let male =0, female = 0
    for (const key in jsonData) {
      
        if(jsonData[key].gender == "male") {
            male++
        }
        if(jsonData[key].gender == "female") {
            female++
        }
    }
    obj1.male = male
    obj1.female = female

    fs.writeFile('homework5- 1_gender.json', JSON.stringify(obj1) , 'utf8', (err) => {
        if(err) {
            console.log(err)
        } else {
            console.log("write success")
        }
    })

    let arr = []
    
    for (const key in jsonData) {
        let obj = {}
        if(jsonData[key].gender == "male") {
            male++
        }
        if(jsonData[key].gender == "female") {
            female++
        }


        obj.id = jsonData[key]._id
        obj.friendCount = jsonData[key].friends.length
        arr.push(obj)
    }

    fs.writeFile('homework5- 1_friends.json', JSON.stringify(arr) , 'utf8', (err) => {
        if(err) {
            console.log(err)
        } else {
            console.log("write success")
        }
    })
})


